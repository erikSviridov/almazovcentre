SELECT DISTINCT vou.Дата_выполнения
    ,CAST(vop.Регистрационный_номер  as nvarchar) as [Регистрационный номер]
    ,LEFT(CONCAT(CAST(vou.Создание_дата_время as date),'T',CAST(vou.Создание_дата_время as time)),19) as дата_создания
    ,CAST(opfw.Имя_объекта_учета as nvarchar) as Имя
FROM almaz_oper.dbo.View_oper_usligiq vou
LEFT JOIN almaz_wh.dbo.obj_patient_wh  vop ON vou.Пациент_хэш = vop.Пациент_хэш
LEFT JOIN almaz_wh.dbo.obj_patientFIO_wh opfw ON vop.Пациент_хэш = opfw.Пациент_хэш
WHERE Код_ОКМУ = 'Q02.02.313'
	AND vou.Создание_дата_время > CAST(%s as datetime)
GROUP BY vou.Дата_выполнения, vop.Регистрационный_номер,vou.Создание_дата_время,opfw.Имя_объекта_учета