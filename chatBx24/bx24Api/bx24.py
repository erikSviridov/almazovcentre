import json
from bx24Api.bitrix24Constructor import bx24Constructor


class bx24(bx24Constructor):
    url = 'https://p.almazovcentre.ru/rest/7703/5zfou9fuqqtdsavb/%s.json'

    def getDataById(self, elementId=21716, blocId=96):
        params = {
            "IBLOCK_TYPE_ID": "lists",
            "IBLOCK_ID": blocId,
            "ELEMENT_ID": elementId
        }

        r = super().buildQuery('lists.element.get', params)

        r.encoding = "utf-8"
        data = json.loads(r.text)
        questionnary = data['result'][0]['PROPERTY_639']['84440']
        userId = data['result'][0]['CREATED_BY']

        return questionnary, userId

    def getDataByDate(self, sendingTime, blocId=96):
        params = {
            "IBLOCK_TYPE_ID": "lists",
            "IBLOCK_ID": blocId,
            'FILTER': {
                '>=DATE_CREATE': sendingTime
            }
        }

        r = super().buildQuery('lists.element.get', params)

        r.encoding = "utf-8"
        data = json.loads(r.text)

        patientsAnswer = None
        patiensId = None

        for i in data:
            patientsAnswer += i['result'][0]['PROPERTY_639']['84440']
            patiensId += ['result'][0]['CREATED_BY']

        return data

    def getEmail(self, userId):
        params = {
            "ID": userId
        }

        r = super().buildQuery('user.get', params)

        r.encoding = "utf-8"
        data = json.loads(r.text)
        email = data['result'][0]['EMAIL']

        return email

    def sendTextMessage(self, chatId, message):
        params = {
            "BOT_ID" : 18175,
            "CLIENT_ID" : "d4zh32797ga6sbk4v0s9iyyowgb4ib9y",
            "FROM_USER_ID": 18175,
            "TO_USER_ID" : chatId,
            "DIALOG_ID": chatId,
            "MESSAGE": message,
        }

        r = super().buildQuery('imbot.message.add', params)
        print(r.text)
