from abc import ABC
import requests


class bx24Constructor(ABC):
    url = 'https://p.almazovcentre.ru/rest/7703/5zfou9fuqqtdsavb/%s.json'

    def buildQuery(self, method, params):

        try:
            session = requests.Session()
            r = session.get(self.url % (method,), params=params)
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)

        return r


