import json
import os
from sqlWorker import sqlWorker
from bx24Api.bx24 import bx24

date = json.loads(open('data.dat', 'r').read())['date']
fileName = 'newHospitalizationDate.sql'

scriptFile = open(os.getcwd() + '\\sqlScripts\\' + fileName, 'r', encoding="utf-8")
script = scriptFile.read()
scriptFile.close()

newConnection = sqlWorker()
newConnection.createConnection()
df = newConnection.getData(script, date)
newConnection.closeConnection()

i = 0
while (i < df.shape[0]):
    message = "Вы назначили пациенту: " + df.iloc[i, 3] + " (рег.№" + df.iloc[i, 1] + ") госпитализацию"
    bx24().sendTextMessage(3417, message)
    i += 1

newDate = "{ \"date\":\"" +df.iloc[:,2].max() +"\"}"

with open("data.dat", "w") as file:
    file.write(newDate)

