import pandas as pd
import pymssql


class sqlWorker:

    def createConnection(self, serverName = "fmc-qms-02", db = "almaz_oper"):
        try:
            self.conn = pymssql.connect(server=serverName, database=db)
            self.cursor = self.conn.cursor()
        except pymssql.connector.Error as err:
            print(err)

    def closeConnection(self):
        self.cursor.close()

    def getData(self, scriptFile,date):

        self.cursor.execute(scriptFile,(date,))
        columnName = [i[0] for i in self.cursor.description]
        df = pd.DataFrame(self.cursor.fetchall())

        if (len(df) != 0):
            df.columns = columnName


        return df