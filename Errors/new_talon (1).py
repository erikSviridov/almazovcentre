import webbrowser
import datetime

import numpy as np
import pandas as pd
#import datetime as dt
#import matplotlib as mpl
# from flask import Flask
import xlsxwriter
#from flask import request
#import pretty_html_table
import pymssql
form_d = datetime.datetime.now() + datetime.timedelta(days=1)
form_d = form_d.strftime("%Y-%m-%d")

def SQL(date):
    try:
        conn = pymssql.connect(server='fmc-qms-02', database='almaz_wh')
        cursor = conn.cursor()
    except pymssql.connector.Error as err:
        print(err)
    cursor.execute('''with shedule as 
(
select ouw.Услуга, opw.Регистрационный_номер, opw.Пациент_хэш, ouw.Дата_начала_назначения, ouw.ФИО_Специалиста, ouw.Интервал_назначения from almaz_wh.dbo.obj_usluga1860_wh ouw left join almaz_wh.dbo.obj_patient_wh opw on opw.Пациент_хэш = ouw.Пациент_хэш
left join almaz_wh.dbo.obj_usluga186_wh ouw2 on ouw.Пациент_хэш=ouw2.Пациент_хэш and ouw.Код_ОК_МУ = ouw2.Код_ОК_МУ and ouw.Номер_назначения = ouw2.Номер_назначения 
where ouw2.Отмена_назначения_дата_время is NULL
and (ouw.ФИО_Специалиста IN ('План госпитализации на ССХ по ВМП', 'План госпитализации Аритмология Аккуратова', 'План госпитализации отделений РЭХ', 'План госпитализации по сосудистой хирургии',
'План госпитализации на ДССХ', 'План госпитализации ОКиМРдет','ПО ГКК госпитализация', 'Кардио-ССХ',
'План госпитализации НХО1(Филиал)','План госпитализации НХО2(Филиал)', 'План госпитализации НХО3(Филиал)', 'План госпитализации НХО4(Филиал)', 'План госпитализации ОТО')
)
AND ouw.Услуга NOT LIKE '%ОМС%' AND ouw.Услуга NOT IN ('Резервирование', 'Госпитализация ''Терапия'' ', 'Перенос госпитализации на Пархоменко')
and (ouw.Дата_начала_назначения = '{}') ---and '2022-07-15')
/*and (opw.Регистрационный_номер = '8343/A22' or opw.Регистрационный_номер ='57191/A22')*/
)
,talon as(
select Регистрационный_номер, Пациент_хэш, Талон_ВМП, Этап, Статус, Код_профиля, Дата_планируемой_госпитализации, ROW_NUMBER() over(PARTITION by Талон_ВМП order by (case Этап 
when '1 этап' then 1
when 'Направлен в МО на оказание ВМП (1 этап)' then 1
when 'Направлен в МО на оказание ВМП 1 этап' then 1
when 'Направлен в МО на оказание ВМП (2 этап)' then 2
when 'Направлен в МО на оказание ВМП 2 этап' then 2
when '2 этап' then 2
when '3 этап' then 3
when '4 этап' then 4 end) Desc, vdafw.Дата_создания  DESC) as num, 
ROW_NUMBER() over(Partition by Пациент_хэш order by Дата_планируемой_госпитализации desc) as num2
from almaz_econom.dbo.VMP_dataAll_from_wh vdafw 
where (Код_профиля='14.00' or Код_профиля = '14' or Код_профиля = '20' or Код_профиля = '20.00')
/*and (Регистрационный_номер ='61921/A22') or Регистрационный_номер = '57191/A22')*/
)
select * from shedule sh
left join talon ta on ta.Пациент_хэш = sh.Пациент_хэш
ORDER by ta.Талон_ВМП'''.format(form_d))
    row = cursor.fetchall()
    Talon_table = pd.DataFrame(row)
    for i in range(len(Talon_table)):
        try:
            Talon_table[0][i]=Talon_table[0][i].encode('cp1252').decode('cp1251')
        except:
            pass
        try:
            Talon_table[4][i]=Talon_table[4][i].encode('cp1252').decode('cp1251')
        except:
            pass
        try:
            Talon_table[9][i]=Talon_table[9][i].encode('cp1252').decode('cp1251')
        except:
            pass
        try:
            Talon_table[10][i]=Talon_table[10][i].encode('cp1252').decode('cp1251')
        except:
            pass
    conn.close()
    try:
        Talon_table.columns=['Услуга','Регистрационный_номер','Пациент_хэш','Дата_начала_назначения','ФИО_Специалиста','Интервал_назначения','Регистрационный_номер.1','Пациент_хэш.1','Талон_ВМП','Этап','Статус','Код_профиля','Дата_планируемой_госпитализации','num','num2']
    except:
        print('Ошибка. Нет записи на этот день')
    return Talon_table


class talon:
    nomer = str()
    reg_n = str()
    etap = 0
    block_flag = False
    no_talon_flag = False
    date = 0
    usluga = str()
    interval = str()
    fio = str()

    def __init__(self, nomer_inp, reg_n_inp, usluga_inp, interval_inp, fio_inp):
        self.nomer = nomer_inp
        self.reg_n = reg_n_inp
        self.usluga = usluga_inp
        self.interval = interval_inp
        self.fio = fio_inp

    def update_etap(self, etap_inp):
        try:

            if etap_inp.find('1') != -1 and self.etap < 1:
                self.etap = 1
            elif etap_inp.find('2') != -1 and self.etap < 2:
                self.etap = 2
            elif etap_inp.find('3') != -1 and self.etap < 3:
                self.etap = 3
            elif etap_inp.find('4') != -1 and self.etap < 4:
                self.etap = 4
            elif etap_inp == ('Заблокировано'):
                self.block_flag = True
            elif etap_inp == ('Отказано в ВМП'):
                self.block_flag = True
            elif etap_inp == ('Пролечен'):
                self.block_flag = True
            elif etap_inp.find('6') != -1:
                self.block_flag = True
            elif etap_inp.find('5') != -1:
                self.block_flag = True
            elif etap_inp == 'No talon':
                self.no_talon_flag = True
        except:
            self.etap = 0


    def set_date(self, date_inp, etap_inp):
        try:
            if etap_inp.find('3') != -1:
                if self.etap < 3:
                    self.date = date_inp
                elif self.etap == 3:
                    if date_inp > self.date:
                        self.date = date_inp
            if etap_inp.find('4') != -1:
                if self.etap < 4:
                    self.date = date_inp
                elif self.etap == 4:
                    if date_inp > self.date:
                        self.date = date_inp
        except:
            self.etap=0



def evolution_func(Talon_table):
    counter = 0
    Talon_table['Дата_планируемой_госпитализации'] = pd.to_datetime(Talon_table['Дата_планируемой_госпитализации'],
                                                                    format='%Y-%m-%d')
    ind = Talon_table.loc[Talon_table['Этап'].isin([np.nan]), 'Этап'] = 'No talon'
    Class_list = [i for i in range(len(Talon_table))]
    for i in range(len(Talon_table)):
        try:
            if i == 0:
                Class_list[i] = talon(Talon_table['Талон_ВМП'][i], Talon_table['Регистрационный_номер'][i], Talon_table['Услуга'][i], Talon_table['Интервал_назначения'][i], Talon_table['ФИО_Специалиста'][i])
                Class_list[i].set_date(Talon_table['Дата_планируемой_госпитализации'][i], Talon_table['Этап'][i])
                Class_list[i].update_etap(Talon_table['Этап'][i])
                continue
            if Talon_table['Талон_ВМП'][i] is None:
                Class_list[i] = talon(Talon_table['Талон_ВМП'][i], Talon_table['Регистрационный_номер'][i], Talon_table['Услуга'][i], Talon_table['Интервал_назначения'][i], Talon_table['ФИО_Специалиста'][i])
                Class_list[i].set_date(Talon_table['Дата_планируемой_госпитализации'][i], Talon_table['Этап'][i])
                Class_list[i].update_etap(Talon_table['Этап'][i])
                counter = i
                continue
            if Talon_table['Талон_ВМП'][i] == Talon_table['Талон_ВМП'][counter]:
                Class_list[counter].set_date(Talon_table['Дата_планируемой_госпитализации'][i], Talon_table['Этап'][i])
                Class_list[counter].update_etap(Talon_table['Этап'][i])
                continue
            Class_list[i] = talon(Talon_table['Талон_ВМП'][i], Talon_table['Регистрационный_номер'][i], Talon_table['Услуга'][i], Talon_table['Интервал_назначения'][i], Talon_table['ФИО_Специалиста'][i])
            Class_list[i].set_date(Talon_table['Дата_планируемой_госпитализации'][i], Talon_table['Этап'][i])
            Class_list[i].update_etap(Talon_table['Этап'][i])
            counter = i
        except:
            pass
    Sorted_class_list = []
    for x in Class_list:
        if type(x) is talon:
            Sorted_class_list.append(x)
    Not_final_array = pd.DataFrame(
        columns=['Талон_ВМП', 'Интервал_назначения', 'ФИО_Специалиста', 'Услуга', "Рег_номер", "Последний_этап",
                 "Дата_планируемой_госпитализации", 'Rating', 'timing','block flag'], index=[x for x in range(len(Sorted_class_list))])
    f = 0
    for i in Sorted_class_list:

        Not_final_array['Талон_ВМП'][f] = i.nomer
        Not_final_array['Рег_номер'][f] = i.reg_n
        Not_final_array['Услуга'][f] = i.usluga
        Not_final_array['Последний_этап'][f] = i.etap
        Not_final_array['Дата_планируемой_госпитализации'][f] = i.date
        Not_final_array['Интервал_назначения'][f] = i.interval
        Not_final_array['ФИО_Специалиста'][f] = i.fio
        Not_final_array['block flag'][f] = i.block_flag
        try:
            Not_final_array['timing'][f] = pd.to_numeric(Not_final_array['Талон_ВМП'][f][8:13])  # <-------
        except:
            Not_final_array['timing'][f] = 0
        f += 1
    return Not_final_array
# WARNING: Decompyle incomplete

Talon_table = SQL(form_d)
Talon_table['Дата_планируемой_госпитализации'] = pd.to_datetime(Talon_table['Дата_планируемой_госпитализации'], format = '%Y-%m-%d')
fa2 = evolution_func(Talon_table)

uniq = fa2['Рег_номер'].unique()
for i in fa2.index:
    if fa2['block flag'][i]== True:
        fa2['Последний_этап'][i]= 0
    try:
        if fa2['Дата_планируемой_госпитализации'][i] == 0 and fa2['Талон_ВМП'][i][14:16]==form_d[2:4]:
            fa2['Дата_планируемой_госпитализации'][i] =pd.Timestamp(form_d[0:4]+'-01-01T12')
        if fa2['Дата_планируемой_госпитализации'][i] == 0 and fa2['Талон_ВМП'][i][14:16]!=form_d[2:4]:
            fa2['Дата_планируемой_госпитализации'][i] = pd.Timestamp('2000-01-01T12')
    except:
        fa2['Дата_планируемой_госпитализации'][i] = pd.Timestamp(form_d[0:4] + '-01-01T12')
fa3 = pd.DataFrame(columns = fa2.columns)
for i in fa2['Рег_номер'].unique():
    loc = fa2.loc[fa2['Рег_номер']==i]
    loc = loc.loc[loc['Дата_планируемой_госпитализации'] == loc['Дата_планируемой_госпитализации'].max()]
    fa3 = fa3.append(loc)
Usluga_SMP = fa3.loc[fa3['Услуга'].str.find('СМП') != -1]
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.max_colwidth', None)
no_etap = fa3.loc[fa3['Последний_этап'] == 0]
Not_forth_etap = fa3.loc[fa3['Последний_этап'] != 4].loc[fa3['Последний_этап'] != 0]
Date_different = fa3.loc[fa3['Дата_планируемой_госпитализации'] != pd.to_datetime(form_d.replace('-', ''))]
Date_different = Date_different.loc[Date_different['Последний_этап'] != 2].loc[Date_different['Последний_этап'] != 1].loc[Date_different['Последний_этап'] != 0]

writer = pd.ExcelWriter(r"\\172.17.3.44\aggregate\PowerBiAnalize\Ошибки ИФ\Файлы\Ошибки для ЦГ"+'{}.xlsx'.format(form_d), engine='xlsxwriter')
no_etap.to_excel(writer, sheet_name='Нет талона ВМП')
Not_forth_etap.to_excel(writer, sheet_name='Нет 4 этапа')
Usluga_SMP.to_excel(writer, sheet_name='Услуга СМП')
Date_different.to_excel(writer, sheet_name='Отличается дата')
fa3.to_excel(writer, sheet_name='Все пациенты')
writer.save()

"""
app = Flask(__name__)
@app.route('/')
def scrape_and_reformat():
    html_str='''<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-16">
    <title>\xd0\xa2\xd0\xb0\xd0\xbb\xd0\xbe\xd0\xbd\xd1\x8b</title>
  </head>

  <body>
    <header>
      <center> <img src="http://www.almazovcentre.ru/wp-content/uploads/logo-new-1.png"

          alt="logo" title="almazov_c_logo" style="width: 159px; height: 159px;">
      </center>
      <h1><center> ФГБУ «НМИЦ им. В. А. Алмазова»</center> </h1>
    </header>
    <div><h2>
      <center>высокий риск что нет талона у:</center>
      </h2>  </div>
    <div><h2>{0}
      <center>в Услуга есть вхождение СМП:</center>
      </h2>  </div>
    <div><h2>{1}
      <center>нет 4 этап у:</center>
      </h2>  </div>
    <div><h2>{2}
      <center>дата планируемой госпитализации отличается от текущей:</center>
      </h2>  </div>{3}
    <div><h2>
      <center>все пациенты:</center>
      </h2>  </div>{4}
  </body>
</html>
'''
    return  html_str.format(pretty_html_table.build_table(no_etap, 'blue_light'), pretty_html_table.build_table(Usluga_SMP, 'blue_light'), pretty_html_table.build_table(Not_forth_etap, 'blue_light'), pretty_html_table.build_table(Date_different, 'blue_light'), pretty_html_table.build_table(fa3.sort_values('ФИО_Специалиста'), 'blue_light'))

if __name__ == '__main__':
    #webbrowser.open('http://127.0.0.1:5000', new=1)
    print('Чтобы закрыть программу, нажмите Ctrl+C')
"""