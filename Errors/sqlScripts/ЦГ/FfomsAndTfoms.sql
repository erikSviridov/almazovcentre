WITH tab as(SELECT
                CAST(vmegv.Номер_эпизода as nvarchar) as Номер_эпизода,
                CASE
                  WHEN RIGHT(LEFT(vmegv.Талон_ВМП,7),4) = '1110' AND vmegv.Вид_оплаты LIKE '%ОМС%'  THEN 1
                   WHEN RIGHT(LEFT(vmegv.Талон_ВМП,7),4) = '0000' AND vmegv.Вид_оплаты LIKE '%ВМП%'  THEN 1
                 ELSE 0
                END as logic,
                CAST(vmegv.Вид_оплаты as nvarchar) as Вид_оплаты,
                vmegv.Талон_ВМП,
                vmegv.Дата_время_поступления,
                CAST(vs.Код_отделения as nvarchar) as Отделение,
                CAST(vs.Должность_ресурс as nvarchar) as Должность,
                CAST(vs.ФИО_Специалиста as nvarchar) as ФИО
                FROM almaz_oper.dbo.View_movementsEpizodGruppaVMP vmegv
                LEFT JOIN almaz_econom.dbo.VMPtalonsSotrudniki vs ON vs.Эпизод_хэш = vmegv.Эпизод_хэш
                WHERE
                (vmegv.Дата_завершения_эпизода IS NULL OR vmegv.Дата_завершения_эпизода >= SYSUTCDATETIME())
                AND vmegv.Талон_ВМП IS NOT NULL AND vmegv.Талон_ВМП <> '00-0000-00000-000' AND vmegv.Номер_эпизода IS NOT NULL)
            SELECT * FROM tab WHERE tab.logic = 0
        	ORDER BY tab.Дата_время_поступления