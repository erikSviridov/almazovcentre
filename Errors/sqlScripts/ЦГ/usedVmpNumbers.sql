SELECT
                CAST(vmegv .Номер_эпизода as nvarchar) as Номер_эпизода,
                vmegv.Талон_ВМП ,
                vnsfw .Талон_ВМП,
                CAST(vnsfw .Номер_эпизода as nvarchar) as Номер_эпизода,
                vmegv.Дата_время_поступления,
                CAST(vs.Код_отделения as nvarchar) as Отделение,
                CAST(vs.Должность_ресурс as nvarchar) as Должность,
                CAST(vs.ФИО_Специалиста as nvarchar) as ФИО
                FROM almaz_oper.dbo.View_movementsEpizodGruppaVMP vmegv
                LEFT JOIN almaz_econom.dbo.View_vmp_nomer_stacionarPosledny_wh  vnsfw ON vnsfw.Талон_ВМП = vmegv.Талон_ВМП
                LEFT JOIN almaz_econom.dbo.VMPtalonsSotrudniki vs ON vs.Эпизод_хэш = vmegv.Эпизод_хэш
                WHERE
                (vmegv.Дата_завершения_эпизода IS NULL OR vmegv.Дата_завершения_эпизода >= SYSUTCDATETIME())
                AND vmegv.Талон_ВМП IS NOT NULL
                AND vmegv .Номер_эпизода <> vnsfw .Номер_эпизода
                AND vmegv.Талон_ВМП <> '00-0000-00000-000'
                AND YEAR(vnsfw.Коррекция_дата_время) = YEAR (SYSUTCDATETIME())
                GROUP BY vmegv .Номер_эпизода,
                vmegv.Талон_ВМП ,
                vnsfw .Талон_ВМП ,
                vnsfw .Номер_эпизода,
                vmegv.Дата_время_поступления,
                vs.Код_отделения,
                vs.Должность_ресурс,
                vs.ФИО_Специалиста
                ORDER BY vmegv.Дата_время_поступления DESC