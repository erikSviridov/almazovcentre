SELECT CAST(oew.Номер_эпизода as nvarchar) as Номер_эпизода
	,CAST(t.[Тип финансирования] as nvarchar) as [Тип финансирования]
	,CAST(t.[Код финансирования] as nvarchar) as [Код финансирования]
	,CAST(си.Наименование as nvarchar) as Наименование
	,CAST(си.Вид_лечения as nvarchar) as  Вид_лечения
FROM almaz_econom.dbo.Спр_ИФ си
RIGHT JOIN almaz_econom.dbo.View_IF t ON CONCAT(t.[Код финансирования], t.[Тип финансирования]) = си.Код_2 AND t.[Признак года] = YEAR(SYSUTCDATETIME())
LEFT JOIN almaz_wh.dbo.obj_epizod_wh oew ON oew.Эпизод_хэш = t.Эпизод_хэш
WHERE CAST(oew.Дата_завершения_эпизода as date) = CAST(DATEADD(DAY,-1, SYSUTCDATETIME()) as date)
    AND t.[Код финансирования] NOT IN ('96')
    AND oew.Вид_поступления IN ('СТАЦИОНАРНО','АКУШЕРСТВО','НОВОРОЖДЕННЫЙ')
	AND си.год = YEAR(SYSUTCDATETIME())
	AND (си.Вид_лечения LIKE 'хир%' OR си.Вид_лечения LIKE 'комб%' OR си.Вид_лечения LIKE 'эндо%')
	AND YEAR(oew.Дата_завершения_эпизода) =YEAR(SYSUTCDATETIME())
	AND (SELECT COUNT(*)
		FROM almaz_econom.dbo.klyuchevie_vmeshatelstva kv
		INNER JOIN almaz_wh.dbo.year_services_1860 ys ON ys.Код_ОК_МУ = kv.Код_ОК_МУ
			AND ys.Состояние_назначения = 'выполнено'
			AND ys.Ключ = YEAR(SYSUTCDATETIME())
		WHERE kv.Тип_финансирования = t.[Тип финансирования] AND kv.Код_финансирования = t.[Код финансирования] AND ys.Эпизод_хэш = t.Эпизод_хэш ) = 0

