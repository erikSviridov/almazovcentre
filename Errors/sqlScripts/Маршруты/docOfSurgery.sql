WITH t as (
	SELECT vi.Эпизод_хэш
		,vi.[Код финансирования]
		,CASE vi.[Тип финансирования]
			WHEN 'ФФОМС: СМП' THEN 'СМП'
			WHEN 'ФФОМС: ВМП' THEN 'ВМП'
			WHEN 'ТФОМС: СМП' THEN 'СМП'
			WHEN 'ТФОМС: ВМП' THEN 'ВМП'
			ELSE vi.[Тип финансирования]
		END as [Тип финансирования]
	FROM almaz_econom.dbo.View_IF vi
	LEFT JOIN almaz_wh.dbo.obj_epizod_wh oew ON oew.Эпизод_хэш = vi.Эпизод_хэш
	WHERE vi.[Признак года] = YEAR(SYSUTCDATETIME())
	    AND vi.[Код финансирования] NOT IN ('96')
	    AND oew.Дата_завершения_эпизода IS NOT NULL
), t2 as (
	SELECT CAST(oew.Номер_эпизода as nvarchar) as Номер_эпизода
		,CAST(t.[Тип финансирования] as nvarchar) as [Тип финансирования]
		,CAST(t.[Код финансирования] as nvarchar) as [Код финансирования]
		,CAST(си.Наименование as nvarchar) as Наименование
		,CAST(си.Вид_лечения as nvarchar) as  Вид_лечения
		,(SELECT COUNT(*)
		FROM almaz_wh.dbo.obj_usluga293_wh ouw
		LEFT JOIN almaz_wh.dbo.year_services_1860 ys ON ys.Услуга186_экз = ouw.Услуга186_экз
		LEFT JOIN almaz_wh.dbo.dir_Resurs dr ON dr.Код_экземпляра_хэш = ys.Кто_выполнил_хэш
		WHERE ouw.Ключ = YEAR(SYSUTCDATETIME()) AND
			(ouw.Статус like 'ПРОТОКОЛ ОПЕРАЦИИ%' OR ouw.Статус like 'ПРОТОКОЛ КЕСАРЕВА СЕЧЕНИЯ')
			AND ouw.Эпизод_хэш = t.Эпизод_хэш) as Протокол_операции
		,(SELECT COUNT(*)
			FROM almaz_wh.dbo.obj_usluga293_wh ouw
			LEFT JOIN almaz_wh.dbo.year_services_1860 ys ON ys.Услуга186_экз = ouw.Услуга186_экз
			LEFT JOIN almaz_wh.dbo.dir_Resurs dr ON dr.Код_экземпляра_хэш = ys.Кто_выполнил_хэш
			WHERE ouw.Ключ = YEAR(SYSUTCDATETIME()) AND
				ouw.Статус like 'ПРОТОКОЛ АНЕСТЕЗИИ'
				AND ouw.Эпизод_хэш = t.Эпизод_хэш) Протокол_анестезии
	FROM almaz_econom.dbo.Спр_ИФ си
	RIGHT JOIN t ON CONCAT(t.[Код финансирования], t.[Тип финансирования]) = си.Код_2
	LEFT JOIN almaz_wh.dbo.obj_epizod_wh oew ON oew.Эпизод_хэш = t.Эпизод_хэш
	WHERE си.год = YEAR(SYSUTCDATETIME())
	    AND CAST(oew.Дата_завершения_эпизода as date) = CAST(DATEADD(DAY,-1, SYSUTCDATETIME()) as date)
	    AND (си.Вид_лечения LIKE 'хир%' OR си.Вид_лечения LIKE 'комб%' OR си.Вид_лечения LIKE 'эндо%')
	    AND oew.Вид_поступления IN ('СТАЦИОНАРНО','АКУШЕРСТВО','НОВОРОЖДЕННЫЙ')
	    AND YEAR(oew.Дата_завершения_эпизода) =YEAR(SYSUTCDATETIME())
)
SELECT * FROM t2 WHERE t2.Протокол_анестезии = 0 OR t2.Протокол_операции = 0

