SELECT CAST(oe.Номер_эпизода as nvarchar) as Номер_эпизода
	,oe.Дата_поступления
	,oe.Дата_завершения_эпизода
	,CAST(oe.ИФ_полный as nvarchar) as ИФ
FROM almaz_wh.dbo.obj_epizod_wh oe
WHERE oe.Порядок_госпитализации NOT LIKE 'планово'
	AND (oe.Дата_завершения_эпизода  >= CONVERT (DATE, SYSDATETIME())  OR oe.Дата_завершения_эпизода  IS NULL)
	AND oe.Вид_поступления IN ('СТАЦИОНАРНО','АКУШЕРСТВО')
	AND YEAR(oe.Дата_поступления) = YEAR(SYSDATETIME())
	AND oe.Номер_эпизода IS NOT NULL