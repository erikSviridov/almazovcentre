SELECT CAST(oe.Номер_эпизода as nvarchar) as Номер_эпизода
		,oe.Дата_поступления
		,CAST(oe.ИФ_полный as nvarchar) as ИФ
	FROM almaz_wh.dbo.obj_epizod_wh oe
	LEFT JOIN almaz_wh.dbo.obj_patient_wh opw ON oe.Пациент_экз = opw.Пациент_экз
	WHERE  (oe.Дата_завершения_эпизода  >= CONVERT (DATE, SYSDATETIME())  OR oe.Дата_завершения_эпизода  IS NULL)
		AND oe.Вид_поступления IN ('СТАЦИОНАРНО','АКУШЕРСТВО')
		AND YEAR(SYSDATETIME()) - YEAR(opw.Дата_рождения) < 1
		AND oe.ИФ_полный LIKE '%ВМП%'