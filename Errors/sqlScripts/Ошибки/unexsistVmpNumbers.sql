SELECT CAST(vvnspw.Номер_эпизода as nvarchar) as Номер_эпизода
	,vvnspw.Талон_ВМП
	,tab.Талон_ВМП as [Талон ВМП c портала]
	,CAST(tab.Этап as nvarchar) as Этап
	,CAST(tab.ФИО as nvarchar) as ФИО
	,LEFT(RIGHT(vvnspw.Талон_ВМП,3),2)
FROM almaz_econom.dbo.View_vmp_nomer_stacionarPosledny_wh vvnspw
LEFT JOIN (SELECT t.Талон_ВМП
	,t.Этап
	,t.ФИО
	FROM almaz_econom.dbo.View_vmp_and_talons_list t
	WHERE LEFT(RIGHT(t.Талон_ВМП,3),2) = 23
) as tab ON vvnspw.Талон_ВМП = tab.Талон_ВМП
WHERE LEFT(RIGHT(vvnspw.Талон_ВМП,3),2) = 23  AND tab.Талон_ВМП IS NULL

