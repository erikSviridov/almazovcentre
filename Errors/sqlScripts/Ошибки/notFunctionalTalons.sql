SELECT CAST(vvnspw.Номер_эпизода as nvarchar) as Номер_эпизода
	,vvnspw.Талон_ВМП
	,CAST(vvatl.Этап as nvarchar) as Этап
FROM almaz_econom.dbo.View_vmp_and_talons_list vvatl
LEFT JOIN almaz_econom.dbo.View_vmp_nomer_stacionarPosledny_wh vvnspw  ON vvnspw.Талон_ВМП = vvatl.Талон_ВМП
WHERE LEFT(RIGHT(vvnspw.Талон_ВМП,3),2) = 23 AND vvatl.Этап IN ('Отказано в ВМП','Заблокировано')