import os
import sqlite3
from datetime import datetime

import numpy as np
import pymssql
import pandas as pd


class SqlScriptWorkTool:
    fileDirectory = r"\\172.17.3.44\aggregate\PowerBiAnalize\Ошибки ИФ\\Файлы"
    remeberedDf = None

    def __init__(self, serverName='fmc-qms-02', db='almaz_wh'):
        self.rememberFile("Мониторинг")
        self.createConnection(serverName, db)
        for folderName in self.getListOfFiles("sqlScripts"):
            for fileName in self.getListOfFiles("sqlScripts\\" + folderName):
                df = self.getData(fileName, folderName)
                self.createFile(df, fileName[:-4], folderName)
        self.closeConnection()

    def createConnection(self, serverName, db):
        try:
            self.conn = pymssql.connect(server=serverName, database=db)
            self.cursor = self.conn.cursor()
        except pymssql.connector.Error as err:
            print(err)

    def closeConnection(self):
        self.cursor.close()

    def getData(self, fileName, sheeetName):
        scriptFile = open(os.getcwd() + '\\sqlScripts\\' + sheeetName + '\\' + fileName, 'r', encoding="utf-8")
        script = scriptFile.read()
        scriptFile.close()

        self.cursor.execute(script)
        columnName = [i[0] for i in self.cursor.description]
        df = pd.DataFrame(self.cursor.fetchall())

        if (len(df) != 0):
            df.columns = columnName

        return df

    def createFile(self, df, sheetName, fileName):
        dateNow = datetime.now()
        filePath = self.fileDirectory + '\\' + fileName + ' ' + dateNow.strftime(
            "%d%h%Y") + ".xlsx"
        if (fileName == "Мониторинг"):
            df = self.processMonitoring(sheetName, df)
        if (os.path.exists(filePath)):
            with pd.ExcelWriter(filePath, mode='a', engine="openpyxl") as writer:
                df.to_excel(writer, sheet_name=sheetName,  index=False)
        else:
            df.to_excel(filePath, sheet_name=sheetName, index=False)

    def getListOfFiles(self, name):
        listOfFiles = os.listdir(os.getcwd() + '\\' + name)
        return listOfFiles

    def rememberFile(self, fileKeyWord):
        listOfFiles = os.listdir(self.fileDirectory)
        print(listOfFiles)
        for i in listOfFiles:
            if (fileKeyWord in i):
                self.remeberedDf = pd.read_excel(self.fileDirectory + '\\' + i, sheet_name=None)

    def processMonitoring(self, sheetName, data):

        cur = sqlite3.connect(os.path.join(r"\\172.17.3.44\aggregate\PowerBiAnalize\Ошибки ИФ\DB", "DB.db"))

        for nEpizod in self.remeberedDf[sheetName]['Номер_эпизода']:
            check = data[data['Номер_эпизода'] == nEpizod]
            savedRow = self.remeberedDf[sheetName][self.remeberedDf[sheetName]['Номер_эпизода'] == nEpizod]

            indSavedRow = savedRow.index
            isEmptySavedRow = savedRow.isna()
            if check.empty and isEmptySavedRow.loc[indSavedRow[0],'Признак']:
                print(data)
                data = pd.concat([savedRow, data])
                #print(data.loc[check.index[0],'Комментарии'])
                #data.loc[check.index[0],'Комментарии'] = savedRow.loc[indSavedRow[0],'Комментарии']
                #print(data[check.index[0]]['Комментарии'])


            if savedRow.loc[indSavedRow[0], 'Признак'] == 1.0 and not (check.empty):
                data = data.drop(check.index[0])
                cur.execute('INSERT INTO Epizod (Эпизоды) VALUES(?)', [nEpizod])
                cur.commit()

        for nEpizod in data['Номер_эпизода']:
            r = cur.execute('SELECT COUNT(*) FROM Epizod WHERE Эпизоды = ?', [nEpizod])
            check = data[data['Номер_эпизода'] == nEpizod]
            if (r.fetchone()[0] > 0):
                data.drop(check.index)

        cur.close()
        data['Признак'] = np.nan

        comments = self.remeberedDf[sheetName][['Номер_эпизода','Комментарии']]

        data = data.drop(['Комментарии'], axis=1, errors = "ignore")
        data = pd.merge(data, comments, how = 'left', on='Номер_эпизода', right_index=False, left_index=False)



        return data
