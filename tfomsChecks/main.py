import pandas as pd
import pymssql as pymssql
import os
import matplotlib.pyplot as plt
import numpy as np

class main:

    def createConnection(self):
        try:
            self.conn = pymssql.connect(server='fmc-qms-02', database='almaz_wh')
            self.cursor = self.conn.cursor()
        except pymssql.connector.Error as err:
            print(err)

    def closeConnection(self):
        self.cursor.close()

    def getData(self,fileName,params=[]):
        scriptFile = open(os.getcwd()+'\\sqlScripts\\'+fileName,'r',encoding="utf-8")
        scriptText = scriptFile.read()
        scriptFile.close()

        scriptText = str.split(scriptText,';')
        script = scriptText[0]

        if(len(params) != 0):
            for p in params:
                script += '\''+p +'\''+ ','
            script = script[:-1] + ")"

        if(len(scriptText) > 1):
            script += scriptText[1]

        print(script)

        self.cursor.execute(script)
        columnName = [i[0] for i in self.cursor.description]
        df = pd.DataFrame(self.cursor.fetchall())
        df.columns = columnName

        return df

servicesName = pd.read_excel("услуги УЗИ.xlsx",0)
financeCode = pd.read_excel("услуги УЗИ.xlsx",1)

m = main()
m.createConnection()

servicesCode = m.getData("okmu.sql",servicesName["Услуга2017"])
qms = m.getData("qms.sql",servicesCode["Код_ОК_МУ"])
eis = m.getData("eis.sql",financeCode["МЭС"])

m.closeConnection

numInQms = sum(qms['Количество_строк'])
print(eis['Количество_строк'])
numInEis = sum(eis['Количество_строк'])
print('Количество услуг в МИС: ' + str(numInQms))
print('Количество услуг в ЕИС ОМС: '+ str(numInEis))
print('Доля выставленных счетов: ' + str((numInEis)*100 /numInQms))

qmsByMonth = qms[['месяц','Количество_строк']].groupby('месяц').sum()
eisByMonth = eis[['месяц','Количество_строк']].groupby('месяц').sum()
print(qmsByMonth.index)

#axs = [0,1]
fig, ax = plt.subplots()

x = np.arange(9)  # the label locations
width = 0.4

qms = ax.bar(x - width/2, qmsByMonth['Количество_строк'][:9],width, label='qms')
eis = ax.bar(x + width/2, eisByMonth['Количество_строк'][:9],width, label='eis')
plt.title('Simple bar chart')
plt.grid(True)

ax.bar_label(qms, padding=3)
ax.bar_label(eis, padding=3)
fig.tight_layout()

plt.show()