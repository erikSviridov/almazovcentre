SELECT sum(s._Fld3829) as Стоимость_лечения
,count(*)-isnull(sum(ac._Fld3894),0)  as Количество_строк
,r._Fld3911 collate Cyrillic_General_CS_AS as Код_финансирования
,isnull(e.Пациент_хэш,ak.Пациент_хэш) as Пациент_хэш
,MONTH(s._Fld3825) as месяц
FROM [1C-NODE-TESTSB].omc_test_1.dbo._Reference74 r
INNER JOIN [1C-NODE-TESTSB].omc_test_1.dbo._Document3803_VT3821 s ON s._Fld3827RRef=r._IDRRef AND YEAR(s._Fld3825) = YEAR(SYSUTCDATETIME())+2000 AND r._Fld3911 IN ('иУЗИ','иУЗИС','иСтКар')
INNER JOIN [1C-NODE-TESTSB].omc_test_1.dbo._Document3803 d ON d._IDRRef=s._Document3803_IDRRef
LEFT JOIN almaz_wh.dbo.obj_epizod_wh e ON e.Номер_эпизода=d._Fld3850 collate Cyrillic_General_CS_AS
left join almaz_wh.dbo.obj_ambKarta253_wh ak on ak.Номер_амбулаторной_медкарты=d._Fld3850 collate Cyrillic_General_CS_AS
LEFT JOIN [1C-NODE-TESTSB].omc_test_1.dbo._AccumRg3891 ac ON CONVERT(varchar(34), ac._Fld4944RRef, 1) + '.' + CONVERT(varchar(34), ac._Fld3976RRef, 1) = CONVERT(varchar(34), s._Fld3823RRef, 1)
WHERE r.[_Fld3911] IN (;
group by r._Fld3911 collate Cyrillic_General_CS_AS
,isnull(e.Пациент_хэш,ak.Пациент_хэш), MONTH(s._Fld3825)


