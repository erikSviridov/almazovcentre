SELECT  ys.Пациент_хэш
	,CAST(ys.Услуга as nvarchar) as Услуга
	,MONTH(ys.Дата_оказания_услуги) as месяц
	,COUNT(ys.Услуга1860_хэш) as Количество_строк
FROM almaz_wh.dbo.year_services_1860 ys
WHERE ys.Ключ = YEAR(SYSUTCDATETIME())
	AND ys.Состояние_назначения = 'выполнено' AND ys.Вид_поступления = 'АМБУЛАТОРНО'
	AND ys.Тариф = 'ОМС' AND ys.Источник_финансирования NOT LIKE '%ВМП%'
	AND ys.Код_ОК_МУ IN (;
GROUP BY ys.Пациент_хэш
    ,ys.Дата_оказания_услуги
	,ys.Услуга