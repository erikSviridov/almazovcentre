import pandas as pd
import pymssql
import collections
#from collections import multiprocessing
x = pd.read_csv('data3.csv', sep=';')
x=x[0:50000]
print(x)
try:
    conn = pymssql.connect(server='ext-qms-iau01', database='almaz_wh')
    cursor = conn.cursor()
except:
    pass
d= pd.DataFrame(columns = ['Услуга1860_экз','Регион', 'Город', 'Страна', 'Район_города_или_АТО', 'Индекс', 'Населённый_пункт', 'ОКАТО','Округ', 'Улица', 'Проезд', 'Дом', 'Корпус', 'Строение', 'Группа_инвалидности'])
ask = '''SELECT Услуга1860_экз, Регион, Город, Страна, Район_города_или_АТО, Индекс, Населённый_пункт, ОКАТО,Округ, Улица, Проезд, Дом, Корпус, Строение, Группа_инвалидности FROM almaz_wh.dbo.obj_usluga1860_wh ouw 
left join almaz_kdc.dbo.obj_patients158fs_kdc  opw on ouw.Пациент_хэш = opw.Пациент_хэш 
left join almaz_kdc.dbo.obj_patients161fs_kdc  opw2 on ouw.Пациент_хэш = opw2.Пациент_хэш 
where Услуга1860_экз = '{}'
'''

for i in x['Услуга1860_экз']: #x['Услуга1860_экз']
    try:
        cursor.execute(ask.format(i))#.format(i)
        row = cursor.fetchone()
        #print(row)
        row = pd.DataFrame(row).T
        row.columns = ['Услуга1860_экз','Регион', 'Город', 'Страна', 'Район_города_или_АТО', 'Индекс', 'Населённый_пункт', 'ОКАТО','Округ', 'Улица', 'Проезд', 'Дом', 'Корпус', 'Строение', 'Группа_инвалидности']
        #print(row)
        d = d.append(row, ignore_index=True)
    except:
        continue
    #d= d.append(row, ignore_index=True)

try:
    conn.close()
except:
    pass
d.to_csv('adr_disab0-50000.csv', sep=';')